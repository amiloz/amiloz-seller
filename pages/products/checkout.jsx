import Head from 'next/head'
import Checkout from '../../components/Checkout'

const CheckoutPage = () => {
  return (
    <>
      <Head>
        <title>Checkout</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.svg" height="64" width="64" />
      </Head>
      <Checkout />
    </>
  )
}

export default CheckoutPage
