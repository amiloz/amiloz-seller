import styles from './index.module.css'
import axios from 'axios'
import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import { useRouter } from 'next/dist/client/router'
import { Col, Image, Row, Tooltip, OverlayTrigger } from 'react-bootstrap'
import moment from 'moment'
import { useDispatch } from 'react-redux'
import {
  removeProduct,
  setInitialState,
  updateOrders,
  updateQuantity,
} from '../../store/slices/productSlice'
import Select from 'react-select'
import withAuth from '../../hoc/withAuth'

function Checkout() {
  const [stores, setStores] = useState([])
  const [selectedStore, setSelectedStore] = useState('')
  const [storeType, setStoreType] = useState('')
  const [windowWidth, setWindowWidth] = useState(0)
  const [loading, setLoading] = useState(false)
  const [storeLoader, setStoreLoader] = useState(false)
  const [paymentMethod, setPaymentMethod] = useState('')
  const [selectedStoreData, setSelectedStoreData] = useState({
    name: '',
    mobile: '',
    poc_name: '',
    poc_mobile: '',
    street: '',
    city: '',
    state: '',
    country: '',
    postal_code: '',
    description: '',
  })
  const { orders } = useSelector((state) => state.product)
  const router = useRouter()
  const dispatch = useDispatch()

  useEffect(() => {
    setWindowWidth(window.innerWidth)
  }, [])

  //multi select design
  const colourStyles1 = {
    control: (styles, { data, isDisabled, isFocused, isSelected }) => ({
      ...styles,
      padding: '3px 8px',
      background: '#fff',
      border: isFocused
        ? '1px solid #BACCDD'
        : isSelected
        ? '1px solid #BACCDD'
        : '1px solid #BACCDD',
      '&:hover': {
        borderColor: '#BACCDD',
      },
      boxSizing: 'border-box',
      borderRadius: '4px',
      outline: 'none',
      fontSize: '14px',
      boxShadow: isFocused ? 'none' : isSelected ? 'none' : 'none',
    }),
    menuPortal: () => {
      return {
        backgroundColor: '#E0F7FA',
      }
    },
    singleValue: (styles) => {
      return {
        ...styles,
        color: '#2D4C66',
        fontSize: '14px',
        lineHeight: '20px',
        letterpacing: '0.25px',
      }
    },
    indicatorSeparator: (styles) => {
      return {
        display: 'none',
      }
    },
    placeholder: (styles) => {
      return {
        ...styles,
        color: '#BACCDD',
        fontSize: '14px',
        lineHeight: '20px',
        letterSpacing: '0.25px',
      }
    },
    // option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    //   return {
    //     ...styles,
    //     backgroundColor: isDisabled
    //       ? null
    //       : isSelected
    //       ? '#E0F7FA'
    //       : isFocused
    //       ? '#E0F7FA'
    //       : null,
    //     color: isDisabled
    //       ? null
    //       : isSelected
    //       ? '#000'
    //       : isFocused
    //       ? '#000'
    //       : null,
    //     '&:hover': {
    //       backgroundColor: '#E0F7FA',
    //     },
    //     cursor: isDisabled ? 'not-allowed' : 'default',

    //     ':active': {
    //       ...styles[':active'],
    //       backgroundColor: !isDisabled && (isSelected ? '#BACCDD' : '#BACCDD'),
    //       color: '#000',
    //     },
    //   }
    // },
  }

  const getAllStores = () => {
    axios({
      method: 'GET',
      url: `/stores`,
    }).then((res) => setStores(res.data.stores))
  }
  const multiselectLocationChangeHandler = (selected) => {
    setSelectedStore(selected.value)
    const myStore = stores.find(
      (store) => store.id === parseInt(selected.value)
    )
    setSelectedStoreData(myStore)
    setStoreType('edit')
  }
  useEffect(() => {
    getAllStores()
  }, [])
  const submitHandler = () => {
    if (selectedStoreData.loaned_amount > 600) {
      toast(`Store's loaned amount exceeds $600`)
      setLoading(false)
      return false
    }
    setLoading(true)
    axios({
      method: 'POST',
      url: `/sales-reps/orders`,
      data: {
        payment_method: paymentMethod,
        store_id: selectedStore,
        orders: orders?.map((order) => {
          return {
            ...order,
            products: order.products?.map((prod) => {
              return {
                product_id: prod.id,
                quantity: prod.quantity,
              }
            }),
          }
        }),
      },
    })
      .then((res) => {
        toast('Order created successfully')
        router.push('/order')
        setLoading(false)
        localStorage.setItem('orders', JSON.stringify([]))
        dispatch(setInitialState())
      })
      .catch((err) => {
        toast('Error creating Order')
        setLoading(false)
      })
  }
  const allProductsArray = orders?.map((order) => order.products)
  const allProducts = allProductsArray?.reduce(function (a, b) {
    return a.concat(b)
  }, [])
  let totalProductsPrice = 0
  let totalProductsQuantity = 0
  const singleOrderPrice = allProductsArray?.map((arr) => {
    let totalSingleProductsPrice = 0
    let totalSingleProductsQuantity = 0
    for (let i = 0; i < arr.length; i++) {
      totalSingleProductsPrice += arr[i].quantity * arr[i].selling_price
      totalSingleProductsQuantity += arr[i].quantity
    }
    const obj = {
      price: totalSingleProductsPrice,
      quantity: totalSingleProductsQuantity,
    }
    return obj
  })
  for (let i = 0; i < allProducts.length; i++) {
    totalProductsPrice += allProducts[i].quantity * allProducts[i].selling_price
    totalProductsQuantity += allProducts[i].quantity
  }
  const selectedStoreDataChangeHandler = (e) => {
    const { name, value } = e.target
    const storeData = { ...selectedStoreData, [name]: value }
    setSelectedStoreData(storeData)
  }
  const storeDataSave = (storeId) => {
    const {
      name,
      mobile,
      poc_name,
      poc_mobile,
      street,
      city,
      state,
      country,
      description,
      postal_code,
    } = selectedStoreData
    if (mobile.toString().length !== 10) {
      toast('Invalid mobile number')
      setStoreLoader(false)
      return false
    }
    if (
      poc_mobile !== null &&
      poc_mobile !== '' &&
      poc_mobile.toString().length !== 10
    ) {
      toast('Invalid person mobile number')
      setStoreLoader(false)
      return false
    }
    setStoreLoader(true)
    axios({
      url: `/stores/${storeId}`,
      method: `PUT`,
      data: {
        name,
        mobile: mobile.toString(),
        poc_name: poc_name === '' ? null : poc_name,
        poc_mobile: poc_mobile === '' ? null : poc_mobile.toString(),
        street,
        city,
        state,
        country,
        description: description === '' ? null : description,
        postal_code: postal_code.toString(),
      },
    })
      .then((res) => {
        getAllStores()
        toast('Store changes saved')
        setStoreLoader(false)
      })
      .catch((err) => {
        toast('Error saving changes')
        setStoreLoader(false)
      })
  }
  const storeDataNew = () => {
    const {
      name,
      mobile,
      poc_name,
      poc_mobile,
      street,
      city,
      state,
      country,
      description,
      postal_code,
    } = selectedStoreData
    if (mobile.toString().length !== 10) {
      toast('Invalid mobile number')
      setStoreLoader(false)
      return false
    }
    if (
      poc_mobile !== null &&
      poc_mobile !== '' &&
      poc_mobile.toString().length !== 10
    ) {
      toast('Invalid person mobile number')
      setStoreLoader(false)
      return false
    }
    setStoreLoader(true)
    axios({
      url: `/stores`,
      method: `POST`,
      data: {
        name,
        mobile: mobile.toString(),
        poc_name: poc_name === '' ? null : poc_name,
        poc_mobile: poc_mobile === '' ? null : poc_mobile.toString(),
        street,
        city,
        state,
        country,
        description: description === '' ? null : description,
        postal_code: postal_code.toString(),
      },
    })
      .then((res) => {
        toast('Store Saved successfully')
        getAllStores()
        setSelectedStore(res.data.store.id)
        setStoreLoader(false)
      })
      .catch((err) => {
        toast('Error saving changes')
        setStoreLoader(false)
      })
  }
  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      10 digit mobile number without + and dashes
    </Tooltip>
  )
  return (
    <div className={styles.Container}>
      <button
        className={styles.redirectButton}
        onClick={() => router.push('/products')}
      >
        Regresar a lista productos
      </button>
      <div className={styles.orderPlaceDiv}>
        <h2 className={styles.orderPlaceDivHeading}>Resumen del pedido</h2>
        <Row>
          <Col lg={6} md={12}>
            <div>
              <div style={{ display: 'flex' }}>
                <div style={{ width: '45%' }}>
                  <label className={styles.modalLabel}>
                    Seleccionar cliente ya registrado
                  </label>
                  <div>
                    <Select
                      onChange={(selected) =>
                        multiselectLocationChangeHandler(selected)
                      }
                      options={stores?.map((store) => {
                        return { value: store.id, label: store.name }
                      })}
                      styles={colourStyles1}
                      placeholder="Select"
                      isClearable={false}
                      value={
                        selectedStore
                          ? {
                              value: selectedStore,
                              label: selectedStoreData.name,
                            }
                          : ''
                      }
                    />
                  </div>
                </div>
                <div style={{ margin: '7% 4% 0 4%' }}>O</div>
                <div style={{ width: '45%' }}>
                  <label className={styles.modalLabel}> </label>
                  <div>
                    <button
                      type="button"
                      onClick={() => {
                        setStoreType('add')
                        setSelectedStoreData({
                          name: '',
                          mobile: '',
                          poc_name: '',
                          poc_mobile: '',
                          street: '',
                          city: '',
                          state: '',
                          country: '',
                          postal_code: '',
                          description: '',
                        })
                        setSelectedStore('')
                      }}
                      className={styles.saveChangesButton}
                      style={{ width: '100%', marginBottom: '0' }}
                    >
                      Agregar nueva tienda
                    </button>
                  </div>
                </div>
              </div>
              {storeType && (
                <div>
                  <Row>
                    <Col>
                      <label className={styles.modalLabel}>
                        Nombre de la tienda{' '}
                        <span style={{ color: '#fc5447' }}>*</span>
                      </label>
                      <input
                        type="text"
                        placeholder="Enter store name"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.name}
                        required
                        className={styles.modalInput}
                        name="name"
                      />
                    </Col>
                    <Col>
                      <label className={styles.modalLabel}>
                        Contacto de la tienda{' '}
                        <span style={{ color: '#fc5447' }}>*</span>
                        <OverlayTrigger
                          placement="right"
                          delay={{ show: 250, hide: 400 }}
                          overlay={renderTooltip}
                        >
                          <span
                            style={{
                              background: '#fc5447',
                              padding: '0 5px',
                              borderRadius: '50%',
                              marginLeft: '20px',
                              color: 'white',
                            }}
                          >
                            ?
                          </span>
                        </OverlayTrigger>
                      </label>
                      <input
                        type="text"
                        placeholder="Enter store contact number"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.mobile}
                        required
                        className={styles.modalInput}
                        name="mobile"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <label className={styles.modalLabel}>
                        Nombre de la persona
                      </label>
                      <input
                        type="text"
                        placeholder="Enter name"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.poc_name}
                        className={styles.modalInput}
                        name="poc_name"
                      />
                    </Col>
                    <Col>
                      <label className={styles.modalLabel}>
                        Persona de contacto{' '}
                        <OverlayTrigger
                          placement="right"
                          delay={{ show: 250, hide: 400 }}
                          overlay={renderTooltip}
                        >
                          <span
                            style={{
                              background: '#fc5447',
                              padding: '0 5px',
                              borderRadius: '50%',
                              marginLeft: '20px',
                              color: 'white',
                            }}
                          >
                            ?
                          </span>
                        </OverlayTrigger>
                      </label>
                      <input
                        type="text"
                        placeholder="Enter contact number"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.poc_mobile}
                        className={styles.modalInput}
                        name="poc_mobile"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <label className={styles.modalLabel}>
                        Calle <span style={{ color: '#fc5447' }}>*</span>
                      </label>
                      <input
                        type="text"
                        placeholder="Enter street"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.street}
                        required
                        className={styles.modalInput}
                        name="street"
                      />
                    </Col>
                    <Col>
                      <label className={styles.modalLabel}>
                        Ciudad <span style={{ color: '#fc5447' }}>*</span>
                      </label>
                      <input
                        type="text"
                        placeholder="Enter city"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.city}
                        required
                        className={styles.modalInput}
                        name="city"
                      />
                    </Col>
                    <Col>
                      <label className={styles.modalLabel}>
                        Estado <span style={{ color: '#fc5447' }}>*</span>
                      </label>
                      <input
                        type="text"
                        placeholder="Enter state"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.state}
                        required
                        className={styles.modalInput}
                        name="state"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <label className={styles.modalLabel}>
                        País <span style={{ color: '#fc5447' }}>*</span>
                      </label>
                      <input
                        type="text"
                        placeholder="Enter country"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.country}
                        required
                        className={styles.modalInput}
                        name="country"
                      />
                    </Col>
                    <Col>
                      <label className={styles.modalLabel}>
                        Código postal{' '}
                        <span style={{ color: '#fc5447' }}>*</span>
                      </label>
                      <input
                        type="text"
                        placeholder="Enter postal code"
                        onChange={(e) => selectedStoreDataChangeHandler(e)}
                        value={selectedStoreData.postal_code}
                        required
                        className={styles.modalInput}
                        name="postal_code"
                      />
                    </Col>
                  </Row>
                  <label className={styles.modalLabel}>Descripción</label>
                  <textarea
                    placeholder="Enter description"
                    onChange={(e) => selectedStoreDataChangeHandler(e)}
                    value={selectedStoreData.description}
                    required
                    className={styles.modalInput}
                    name="description"
                  />
                  <button
                    type="button"
                    onClick={() =>
                      storeType === 'edit'
                        ? storeDataSave(selectedStoreData.id)
                        : storeDataNew()
                    }
                    className={styles.saveChangesButton}
                    disabled={storeLoader}
                    style={storeLoader ? { background: '#e7e7e7' } : {}}
                  >
                    Guardar cambios
                  </button>
                </div>
              )}
            </div>
          </Col>
          <Col>
            <div className={styles.placeOrderDiv}>
              {windowWidth > 800 ? (
                <>
                  <label className={styles.modalLabel}>Forma de pago</label>
                  <select
                    className={styles.modalInput}
                    style={{
                      width: '60%',
                      display: 'block',
                      margin: '0 auto',
                    }}
                    value={paymentMethod}
                    onChange={(e) => setPaymentMethod(e.target.value)}
                  >
                    <option hidden>- select -</option>
                    <option selected disabled>
                      - select -
                    </option>
                    <option value="Full Credit">Full Credit</option>
                    <option value="Partial Payment">Partial Payment</option>
                    <option value="Full Payment">Full Payment</option>
                  </select>
                  <button
                    onClick={() => submitHandler()}
                    className={styles.addToCartButton}
                    disabled={
                      selectedStore === '' ||
                      loading ||
                      paymentMethod === '' ||
                      orders.length === 0
                    }
                    style={
                      selectedStore === '' ||
                      loading ||
                      paymentMethod === '' ||
                      orders.length === 0
                        ? {
                            background: '#e7e7e7',
                          }
                        : {}
                    }
                  >
                    Realizar pedido
                  </button>
                  <div className={styles.placeOrderDivPrice}>
                    Total ({totalProductsQuantity} elementos) :{' '}
                    <span style={{ fontWeight: '500' }}>
                      ${totalProductsPrice}
                    </span>
                  </div>
                </>
              ) : (
                <>
                  <label className={styles.modalLabel}>Forma de pago</label>
                  <select
                    className={styles.modalInput}
                    style={{
                      width: '60%',
                      display: 'block',
                      margin: '0 auto',
                    }}
                    value={paymentMethod}
                    onChange={(e) => setPaymentMethod(e.target.value)}
                  >
                    <option hidden>- select -</option>
                    <option selected disabled>
                      - select -
                    </option>
                    <option value="Full Credit">Full Credit</option>
                    <option value="Partial Payment">Partial Payment</option>
                    <option value="Full Payment">Full Payment</option>
                  </select>
                  <div className={styles.placeOrderDivPrice}>
                    Total ({totalProductsQuantity} elementos) :{' '}
                    <span style={{ fontWeight: '500' }}>
                      ${totalProductsPrice}
                    </span>
                  </div>
                  <button
                    onClick={() => submitHandler()}
                    className={styles.addToCartButton}
                    disabled={
                      selectedStore === '' ||
                      loading ||
                      paymentMethod === '' ||
                      orders.length === 0
                    }
                    style={
                      selectedStore === '' ||
                      loading ||
                      paymentMethod === '' ||
                      orders.length === 0
                        ? {
                            background: '#e7e7e7',
                          }
                        : {}
                    }
                  >
                    Realizar pedido
                  </button>
                </>
              )}
            </div>
          </Col>
        </Row>
      </div>
      {orders?.map((order, index) => {
        const { date, products } = order
        return (
          <div key={date} className={styles.productList}>
            <h2 className={styles.productListDate}>
              <Row>
                <Col lg={6} md={12}>
                  {moment(date).format('LL')}{' '}
                </Col>
                <Col lg={6} md={12}>
                  <span style={{ float: 'right', fontSize: '18px' }}>
                    Total ({singleOrderPrice[index].quantity} elementos) : $
                    {singleOrderPrice[index].price}
                  </span>
                </Col>
              </Row>
            </h2>
            <Row>
              {products?.map((prod, index) => {
                const { id, images, name, selling_price, quantity } = prod
                return (
                  <Col key={id} lg={4} style={{ marginBottom: '15px' }}>
                    <div className={styles.productItem}>
                      <div style={{ marginRight: '20px' }}>
                        <Image
                          src={images[0].url}
                          alt=""
                          width="120px"
                          height="120px"
                        />
                      </div>
                      <div>
                        <h3
                          className={styles.productItemName}
                          style={{ display: 'inline-block' }}
                        >
                          {name}
                        </h3>
                        <button
                          className={styles.productItemRemoveButton}
                          onClick={() =>
                            dispatch(
                              removeProduct({
                                date,
                                productId: id,
                              })
                            )
                          }
                          type="button"
                        >
                          <i className="fa fa-trash" aria-hidden="true"></i>
                        </button>
                        <p className={styles.productItemPrice}>
                          ${parseInt(selling_price) * parseInt(quantity)}
                        </p>
                        <div className={styles.quantityDiv}>
                          <button
                            onClick={() =>
                              dispatch(
                                updateQuantity({
                                  date,
                                  productId: id,
                                  action: 'minus',
                                })
                              )
                            }
                            disabled={quantity === 1}
                            className={styles.quantityButton}
                          >
                            -
                          </button>
                          <span>{quantity}</span>
                          <button
                            onClick={() =>
                              dispatch(
                                updateQuantity({
                                  date,
                                  productId: id,
                                  action: 'add',
                                })
                              )
                            }
                            className={styles.quantityButton}
                          >
                            +
                          </button>
                        </div>
                      </div>
                    </div>
                  </Col>
                )
              })}
            </Row>
          </div>
        )
      })}
    </div>
  )
}

export default withAuth(Checkout)
