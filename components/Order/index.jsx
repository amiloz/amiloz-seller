import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap'
import styles from './index.module.css'
import moment from 'moment'
import OrderDeleteModal from './OrderDeleteModal'
import OrderEditModal from './OrderEditModal'
import DatePicker from 'react-datepicker'
import DropdownTask from './DropdownTask'
import { useRouter } from 'next/dist/client/router'
import Pagination from 'react-js-pagination'
import withAuth from '../../hoc/withAuth'

const Orders = () => {
  const [showEditModal, setShowEditModal] = useState(false)
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [industries, setIndustries] = useState([])
  const [selectedName, setSelectedName] = useState({})
  const [displayFilterMenu, setDisplayFilterMenu] = useState(false)
  const [displaySortMenu, setDisplaySortMenu] = useState(false)
  const [filterKey, setFilterKey] = useState(null)
  const [sortKey, setSortKey] = useState(null)
  const [sortDir, setSortDir] = useState(true)
  const [date, setDate] = useState('')
  const [stores, setStores] = useState([])
  const [storeId, setStoreId] = useState('')
  const [windowWidth, setWindowWidth] = useState(0)
  const [products, setProducts] = useState([])
  const [activePage, setActivePage] = useState(1)
  const [total, setTotal] = useState(0)
  const router = useRouter()

  useEffect(() => {
    setWindowWidth(window.innerWidth)
  }, [])

  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplayFilterMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const wrapperRef = useRef(null)
  useOutsideAlerter(wrapperRef)
  function useOutsideAlerterSort(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplaySortMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const sortRef = useRef(null)
  useOutsideAlerterSort(sortRef)

  const getAllIndusries = (
    dateValue,
    page = 1,
    filterKeyValue,
    sortKeyValue,
    sortDirection
  ) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/sales-reps/orders?_limit=30&_page=${page}`
    if (dateValue) {
      url += `&date=${moment(dateValue).format('YYYY-MM-DD')}`
    }
    if (filterKeyValue) {
      url += `&store_id=${filterKeyValue}`
    }
    if (sortKeyValue) {
      const dir = sortDirection === true ? 'desc' : 'asc'
      url += `&sort_by=${sortKeyValue}&sort_dir=${dir}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setIndustries(res.data.orders)
      setTotal(res.data.total)
    })
  }
  const getAllStores = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/stores`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setStores(res.data.stores))
  }
  const getAllProductsInitial = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/products?_limit=30&_page=1`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setProducts(res.data.products))
  }
  const getAllProducts = (e) => {
    const token = JSON.parse(localStorage.getItem('token'))
    const { value } = e.target
    let url = `/products?_limit=30&_page=1`
    if (value) {
      url += `&name=${value}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setProducts(res.data.products))
  }
  useEffect(() => {
    getAllIndusries()
    getAllStores()
    getAllProductsInitial()
  }, [])
  const filterKeyChangeHandler = (e) => {
    const { value } = e.target
    if (e.target.checked) {
      setFilterKey(value)
      getAllIndusries(null, 1, value)
    } else {
      setFilterKey(null)
      getAllIndusries(null, 1, null)
    }
    setDisplayFilterMenu(false)
  }
  const sortKeyChangeHandler = (dir, e) => {
    const { value } = e.target
    if (e.target.checked) {
      setSortKey(value)
      setSortDir(dir)
      getAllIndusries(null, 1, filterKey, value, dir)
    } else {
      setSortKey(null)
      getAllIndusries(null, 1, filterKey, null)
    }
    setDisplaySortMenu(false)
  }
  return (
    <div className={styles.Container}>
      <Row>
        <Col>
          <div style={{ position: 'relative' }}>
            <DatePicker
              selected={date}
              onChange={(date) => {
                setDate(date)
                getAllIndusries(date, 1, storeId)
              }}
              placeholderText="Filtrar por fecha"
              className={styles.modalInputDate}
            />
          </div>
        </Col>
        <Col>
          <div className={styles.filterSortDiv}>
            <div>
              {/* <label>Filter by Store</label> */}
              <select
                value={storeId}
                onChange={(e) => {
                  setStoreId(e.target.value)
                  getAllIndusries(date, 1, e.target.value)
                }}
                className={styles.modalInput}
                style={{ height: '32px', paddingTop: '0', paddingBottom: '0' }}
              >
                <option value="">All</option>
                {stores?.map((store) => (
                  <option key={store.id} value={store.id}>
                    {store.name}
                  </option>
                ))}
              </select>
            </div>
            <div className={styles.sortByDiv}>
              <span onClick={() => setDisplaySortMenu(!displaySortMenu)}>
                Ordenar por{' '}
                <span style={{ float: 'right' }}>
                  <i className="fa fa-caret-down" aria-hidden="true"></i>
                </span>
              </span>
              {displaySortMenu ? (
                <ul ref={sortRef}>
                  <li
                    className={
                      sortKey === 'date' && !sortDir && styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="date"
                          checked={sortKey === 'date' && !sortDir}
                          onChange={(e) => sortKeyChangeHandler(false, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Expected Delivery Date (Ascending)
                    </label>
                  </li>
                  <li
                    className={
                      sortKey === 'date' && sortDir && styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="date"
                          checked={sortKey === 'date' && sortDir}
                          onChange={(e) => sortKeyChangeHandler(true, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Expected Delivery Date (Descending)
                    </label>
                  </li>
                </ul>
              ) : null}
            </div>
          </div>
        </Col>
      </Row>
      {windowWidth > 800 ? (
        <div>
          <table className={styles.myTable}>
            <thead>
              <tr>
                <th className={styles.theadTh1}>Pedido Id </th>
                <th className={styles.theadTh1}>Fecha</th>
                <th className={styles.theadTh1}>Fecha estimada de entrega</th>
                <th className={styles.theadTh}>Tiendas</th>
                <th className={styles.theadTh1}>Total</th>
                <th className={styles.theadTh1}>Estatus</th>
                <th className={styles.theadTh1}>Ver detalles</th>
                <th className={styles.theadTh1}>Acción</th>
              </tr>
            </thead>
            <tbody>
              {industries?.map((industry) => {
                const { id, date, grand_total, status, created_at, store } =
                  industry
                return (
                  <tr
                    key={id}
                    className={styles.tbodyTr}
                    style={{ margin: '12px 0' }}
                  >
                    <td className={styles.tbodyTd1}>{id}</td>
                    <td className={styles.tbodyTd1}>
                      {moment(created_at).format('LL')}
                    </td>
                    <td className={styles.tbodyTd1}>
                      {moment(date).format('LL')}
                    </td>
                    <td className={styles.tbodyTd}>
                      <div>
                        <span className={styles.OrderName}>{store.name}</span>
                        <span className={styles.OrderContact}>
                          {store.mobile}
                        </span>
                        <br />
                        <span className={styles.OrderName}>
                          {store.poc_name}
                        </span>
                        <span className={styles.OrderContact}>
                          {store.poc_mobile}
                        </span>
                        <br />
                        <span className={styles.OrderContact}>
                          {store.street}, {store.city}, {store.state},{' '}
                          {store.country}
                        </span>
                      </div>
                    </td>
                    <td className={styles.tbodyTd1}>{grand_total}</td>
                    <td className={styles.tbodyTd1}>{status}</td>
                    <td className={styles.tbodyTd1}>
                      <button
                        onClick={() => {
                          router.push(`/order/${id}`)
                        }}
                        className={styles.buttonEditProd}
                      >
                        Ver detalles
                      </button>
                    </td>
                    <td className={styles.tbodyTd1}>
                      <button
                        onClick={() => {
                          setShowEditModal(true)
                          setSelectedName(industry)
                        }}
                        disabled={
                          !(status === 'Assigned' || status === 'Placed')
                        }
                        style={
                          status === 'Assigned' || status === 'Placed'
                            ? {}
                            : { background: '#e7e7e7', cursor: 'not-allowed' }
                        }
                        className={styles.buttonEditProd}
                      >
                        Editar
                      </button>
                      <button
                        onClick={() => {
                          setShowDeleteModal(true)
                          setSelectedName(industry)
                        }}
                        disabled={status === 'Cancelled'}
                        style={
                          status === 'Cancelled'
                            ? { background: '#e7e7e7', cursor: 'not-allowed' }
                            : {}
                        }
                        className={styles.buttonDeleteProd}
                      >
                        Cancelar
                      </button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      ) : (
        <>
          <div>
            {industries?.map((industry) => {
              const { id, date, grand_total, status, store } = industry
              return (
                <div key={id} className={styles.tableMobDiv}>
                  <div className={styles.tableMobDivFlex}>
                    <span className={styles.tableMobDivFlex1}>
                      {moment(date).format('LL')}
                    </span>
                    <span className={styles.tableMobDivFlex2}>
                      <DropdownTask
                        openEditModal={() => {
                          setShowEditModal(true)
                          setSelectedName(industry)
                        }}
                        openDeleteModal={() => {
                          setShowDeleteModal(true)
                          setSelectedName(industry)
                        }}
                        selectedOrder={industry}
                      />
                    </span>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Tiendas: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>
                      <div>
                        <span className={styles.OrderName}>{store.name}</span>
                        <span className={styles.OrderContact}>
                          {store.mobile}
                        </span>
                        <br />
                        <span className={styles.OrderName}>
                          {store.poc_name}
                        </span>
                        <span className={styles.OrderContact}>
                          {store.poc_mobile}
                        </span>
                        <br />
                        <span className={styles.OrderContact}>
                          {store.street}, {store.city}, {store.state},{' '}
                          {store.country}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Total: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>
                      {grand_total}
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Estatus: </span>
                    <div className={styles.tableMobDivDownInfo}>{status}</div>
                  </div>
                </div>
              )
            })}
          </div>
        </>
      )}
      <div
        style={{
          marginTop: '20px',
          borderTop: '1px solid #e7e7e7',
          textAlign: 'right',
          paddingTop: '20px',
          minHeight: '62px',
        }}
      >
        <div style={{ float: 'right' }}>
          <Pagination
            activePage={activePage}
            itemsCountPerPage={30}
            totalItemsCount={total}
            pageRangeDisplayed={5}
            onChange={(page) => {
              setActivePage(page)
              getAllProducts(null, 1, brandId, categoryId, sortKey, sortDir)
            }}
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>
      </div>
      <OrderEditModal
        show={showEditModal}
        onHide={() => setShowEditModal(false)}
        handleClose={() => setShowEditModal(false)}
        getAllIndusries={getAllIndusries}
        selectedIndustry={selectedName}
        stores={stores}
        productsList={products}
        getAllProducts={getAllProducts}
        getAllStores={getAllStores}
      />
      <OrderDeleteModal
        show={showDeleteModal}
        onHide={() => setShowDeleteModal(false)}
        handleClose={() => setShowDeleteModal(false)}
        getAllIndusries={getAllIndusries}
        selectedIndustry={selectedName}
      />
    </div>
  )
}

export default withAuth(Orders)
