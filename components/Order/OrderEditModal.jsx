import axios from 'axios'
import { useEffect, useState, useRef } from 'react'
import { Col, Form, Image, Modal, Row } from 'react-bootstrap'
import styles from './index.module.css'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import StoreChangeModal from './StoreChangeModal'
import { toast } from 'react-toastify'

const OrderEditModal = ({
  show,
  handleClose,
  getAllIndusries,
  selectedIndustry,
  stores,
  productsList,
  getAllProducts,
  getAllStores,
}) => {
  const [loading, setLoading] = useState(false)
  const [date, setDate] = useState(new Date())
  const [storeId, setStoreId] = useState('')
  const [storeData, setStoreData] = useState({})
  const [products, setProducts] = useState([])
  const [displayMenu, setDisplayMenu] = useState(false)
  const [searchInputValue, setSearchInputValue] = useState('')
  const [showModal, setShowModal] = useState(false)

  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplayMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const wrapperRef = useRef(null)
  useOutsideAlerter(wrapperRef)

  useEffect(() => {
    const dateS = selectedIndustry.date && new Date(selectedIndustry?.date)
    const offsetDate =
      dateS === null || dateS === '' || dateS === undefined
        ? new Date()
        : dateS.getTimezoneOffset() < 0
        ? new Date(dateS.getTime() - dateS.getTimezoneOffset() * 60000)
        : new Date(dateS.getTime() + dateS.getTimezoneOffset() * 60000)
    const store_id = selectedIndustry.store && selectedIndustry.store.id
    const store_data = selectedIndustry.store
    const productsArr = selectedIndustry?.products
    setDate(offsetDate)
    setStoreId(store_id)
    setProducts(productsArr)
    setStoreData(store_data)
  }, [selectedIndustry])
  const findStore = () => {
    console.log('hhh')
    const myStore = stores?.find((store) => store.id === parseInt(storeId))
    setStoreData(myStore)
  }
  const submitHandler = (dateParam, storeIdParam) => {
    setLoading(true)
    axios({
      method: 'PUT',
      url: `/sales-reps/orders/${selectedIndustry.id}`,
      data: {
        store_id: storeIdParam,
        date: moment(dateParam).format('YYYY-MM-DD'),
        // products: products?.map((prod) => {
        //   const { id, product_id, quantity } = prod
        //   return {
        //     ...(!prod._new && { id }),
        //     product_id: prod._new ? id : product_id,
        //     quantity,
        //   }
        // }),
      },
    })
      .then((res) => {
        // handleClose()
        getAllIndusries()
        setLoading(false)
        toast('Order edited!')
        const myStore = stores?.find(
          (store) => store.id === parseInt(storeIdParam)
        )
        setStoreData(myStore)
      })
      .catch((err) => {
        console.log(err)
        setLoading(false)
        toast('Error editing order')
      })
  }
  const removeProduct = (id, productId) => {
    const prodArr = products?.filter((prod) => prod.id !== id)
    setProducts(prodArr)
    deleteProductsSubmitHandler(productId)
  }
  const productsAddHandler = (item) => {
    const newProdArr = [
      ...products,
      { ...item, quantity: 1, _new: true, product_id: item.id },
    ]
    setProducts(newProdArr)
    setDisplayMenu(false)
    addProductSubmitHandler(item.id, 1)
  }
  const dateChangeHandler = (dateParam) => {
    submitHandler(dateParam, storeId)
    setDate(dateParam)
  }
  const storeChangeHandler = (storeIdParam) => {
    submitHandler(date, storeIdParam)
    setStoreId(storeIdParam)
  }
  const changeQuantity = (prodId, operation) => {
    let productQuantity = 0
    let productId = ''
    const prodCheck = products?.map((prod) => {
      if (prodId === prod.id) {
        prod.quantity =
          operation === 'add' ? prod.quantity + 1 : prod.quantity - 1
        productQuantity =
          operation === 'add' ? prod.quantity + 1 : prod.quantity - 1
        productId = prod.product_id
      }
      return prod
    })
    setProducts(prodCheck)
    quantitySubmitHandler(productId, productQuantity)
  }
  const selectedProductsArray = products?.map((prod) => prod.id)
  const productsToShowInMenu =
    selectedProductsArray === undefined
      ? []
      : productsList?.filter((prod) => !selectedProductsArray.includes(prod.id))
  const quantitySubmitHandler = (productId, quantity) => {
    axios({
      method: 'PUT',
      url: `/sales-reps/orders/${selectedIndustry.id}/products/${productId}`,
      data: {
        quantity,
      },
    }).then((res) => {
      toast('Order Edited!')
      getAllIndusries()
    })
  }
  const addProductSubmitHandler = (productId, quantity) => {
    axios({
      method: 'POST',
      url: `/sales-reps/orders/${selectedIndustry.id}/products/${productId}`,
      data: {
        quantity,
      },
    }).then((res) => {
      toast('Order Edited!')
      getAllIndusries()
    })
  }
  const deleteProductsSubmitHandler = (productId) => {
    axios({
      method: 'DELETE',
      url: `/sales-reps/orders/${selectedIndustry.id}/products/${productId}`,
      data: {},
    }).then((res) => {
      toast('Order Edited!')
      getAllIndusries()
    })
  }
  return (
    <Modal
      show={show}
      onHide={handleClose}
      fullscreen="md-down"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div className={styles.mobileModalDiv} onClick={() => handleClose()}>
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            Edit Order
          </p>
        </div>
        <h3 className={styles.modalHeadingMain}>Edit Order</h3>
        <Form>
          <Form.Label className={styles.modalLabel}>Order Date</Form.Label>
          <DatePicker
            selected={date}
            onChange={(date) => dateChangeHandler(date)}
            minDate={moment().toDate()}
            placeholderText="Select a day"
            className={styles.modalInput}
          />
          <Form.Label className={styles.modalLabel}>Store</Form.Label>
          {storeData && (
            <div>
              <span className={styles.OrderName}>{storeData.name}</span>
              <span className={styles.OrderContact}>{storeData.mobile}</span>
              <br />
              <span className={styles.OrderName}>{storeData.poc_name}</span>
              <span className={styles.OrderContact}>
                {storeData.poc_mobile}
              </span>
              <br />
              <span className={styles.OrderContact}>
                {storeData.street}, {storeData.city}, {storeData.state},{' '}
                {storeData.country}
              </span>
            </div>
          )}
          <button
            type="button"
            onClick={() => setShowModal(true)}
            className={styles.editButton}
          >
            Edit Store
          </button>
          <Form.Label className={styles.modalLabel}>Products</Form.Label>
          {/*Custom Dropdown*/}
          <div className={styles.customDropdownDiv}>
            <input
              value=""
              className={styles.customDropdownInput}
              placeholder="Select Products"
              onClick={() => {
                setDisplayMenu(true)
              }}
              readOnly
            />
            <Image
              src="/icons/searchIconDark.svg"
              alt=""
              className={styles.inputDivIcon}
              width="17.49px"
              height="17.49px"
            />
            <Image
              src={
                displayMenu
                  ? '/images/CarretUpGrey.png'
                  : '/images/CarretDownGrey.png'
              }
              alt=""
              className={styles.customDropdownIcon}
            />
            {displayMenu ? (
              <ul ref={wrapperRef}>
                <li>
                  <input
                    className={styles.customDropdownSearchInput}
                    placeholder="Select the products to add"
                    value={searchInputValue}
                    onChange={(e) => {
                      getAllProducts(e)
                      setSearchInputValue(e.target.value)
                    }}
                  />
                </li>
                {productsToShowInMenu?.map((type) => (
                  <li key={type.id} onClick={() => productsAddHandler(type)}>
                    {type.name}
                  </li>
                ))}
              </ul>
            ) : null}
          </div>
          <Row>
            {products?.map((prod) => {
              const { id, name, quantity } = prod
              return (
                <Col key={id} lg={4} style={{ marginBottom: '15px' }}>
                  <div className={styles.productItem}>
                    <div>
                      <h3 className={styles.OrderName}>{name}</h3>
                      <div className={styles.quantityDiv}>
                        <button
                          onClick={() => changeQuantity(id, 'minus')}
                          disabled={quantity === 1}
                          className={styles.quantityButton}
                          type="button"
                        >
                          -
                        </button>
                        <span>{quantity}</span>
                        <button
                          onClick={() => changeQuantity(id, 'add')}
                          className={styles.quantityButton}
                          type="button"
                        >
                          +
                        </button>
                      </div>
                    </div>
                    <button
                      className={styles.productItemRemoveButton}
                      onClick={() => removeProduct(id, prod.product_id)}
                      type="button"
                    >
                      <i className="fa fa-trash" aria-hidden="true"></i>
                    </button>
                  </div>
                </Col>
              )
            })}
          </Row>
          {/* <div className={styles.modalButtonDiv}>
            <button
              type="submit"
              className={
                loading ? styles.modalDisabledButton : styles.modalSaveButton
              }
              disabled={loading}
            >
              Save
            </button>
          </div> */}
        </Form>
      </Modal.Body>
      <StoreChangeModal
        show={showModal}
        onHide={() => setShowModal(false)}
        handleClose={() => setShowModal(false)}
        stores={stores}
        setStoreId={setStoreId}
        getAllStores={getAllStores}
        getAllIndusries={getAllIndusries}
        storeId={storeId}
        findStore={findStore}
        storeData={storeData}
        submitHandler={submitHandler}
        orderDate={date}
      />
    </Modal>
  )
}

export default OrderEditModal
