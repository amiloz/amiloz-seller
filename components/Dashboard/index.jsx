import styles from '../Order/index.module.css'
import { useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import moment from 'moment'
import axios from 'axios'
import { useRouter } from 'next/dist/client/router'
import DatePicker from 'react-datepicker'

const Dashboard = () => {
  const [date, setDate] = useState(new Date())
  const [industries, setIndustries] = useState([])
  const [windowWidth, setWindowWidth] = useState(0)
  const { name } = useSelector((state) => state.auth)
  const router = useRouter()
  useEffect(() => {
    setWindowWidth(window.innerWidth)
  }, [])
  const getAllIndusries = (dateFilter) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/sales-reps/orders?_limit=30&_page=1&date=${moment(
      dateFilter
    ).format('YYYY-MM-DD')}`
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setIndustries(res.data.orders)
    })
  }
  useEffect(() => {
    getAllIndusries()
  }, [])
  return (
    <div className={styles.Container}>
      <h2 className={styles.orderPlaceDivHeading}>Bienvenido {name}</h2>
      <div>
        <label className={styles.modalLabel}>Filtrar por fecha</label>
        <DatePicker
          selected={date}
          onChange={(date) => {
            setDate(date)
            getAllIndusries(date)
          }}
          placeholderText="Filtrar por fecha"
          className={styles.modalInputDate}
        />
      </div>
      <Row style={{ margin: '20px 0' }}>
        <Col
          lg={6}
          md={12}
          style={windowWidth > 1000 ? { paddingLeft: '0' } : {}}
        >
          <div className={styles.orderPlaceDiv}>
            <h3 className={styles.detailPageHeadingSub}>Pedidos</h3>
            <div
              style={{
                height: '64vh',
                overflowY: 'auto',
                overflowX: 'hidden',
                paddingRight: '7px',
              }}
            >
              {industries.length === 0 ? (
                <span className={styles.OrderName}>
                  No hay pedidos para mostrar
                </span>
              ) : (
                <table className={styles.myTable}>
                  <thead>
                    <tr>
                      <th className={styles.theadTh}>Tiendas</th>
                      <th className={styles.theadTh1}>Total</th>
                      <th className={styles.theadTh1}>Estatus</th>
                      <th className={styles.theadTh1}>Ver detalles</th>
                    </tr>
                  </thead>
                  <tbody>
                    {industries?.map((industry) => {
                      const { id, grand_total, status, store } = industry
                      return (
                        <tr
                          key={id}
                          className={styles.tbodyTr}
                          style={{ margin: '12px 0' }}
                        >
                          <td className={styles.tbodyTd}>
                            <div>
                              <span className={styles.OrderName}>
                                {store.name}
                              </span>
                              <span className={styles.OrderContact}>
                                {store.mobile}
                              </span>
                              <br />
                              <span className={styles.OrderName}>
                                {store.poc_name}
                              </span>
                              <span className={styles.OrderContact}>
                                {store.poc_mobile}
                              </span>
                              <br />
                              <span className={styles.OrderContact}>
                                {store.street}, {store.city}, {store.state},{' '}
                                {store.country}
                              </span>
                            </div>
                          </td>
                          <td className={styles.tbodyTd1}>{grand_total}</td>
                          <td className={styles.tbodyTd1}>{status}</td>
                          <td className={styles.tbodyTd1}>
                            <button
                              onClick={() => {
                                router.push(`/order/${id}`)
                              }}
                              className={styles.buttonEditProd}
                            >
                              Ver detalles
                            </button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              )}
            </div>
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default Dashboard
