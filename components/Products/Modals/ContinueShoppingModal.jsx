import { useRouter } from 'next/dist/client/router'
import { Col, Image, Modal, Row } from 'react-bootstrap'
import styles from '../index.module.css'

const ContinueShoppingModal = ({ show, handleClose, selectedProduct }) => {
  const { name, images, size, quantity, unit_in_package, weight } =
    selectedProduct
  const router = useRouter()
  return (
    <Modal
      show={show}
      onHide={handleClose}
      fullscreen="md-down"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
      size="lg"
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <Row>
          <Col>
            {images && images.length > 0 && (
              <Image src={images[0].url} alt="" height="170px" width="93%" />
            )}
          </Col>
          <Col>
            <h2>Added to Cart</h2>
            <p>{name}</p>
            <ul>
              <li>Size: {size}</li>
              <li>Quantity: {quantity}</li>
              <li>Weight: {weight}</li>
              <li>Unit in package: {unit_in_package}</li>
            </ul>
          </Col>
        </Row>
        <Row>
          <Col>
            <button
              onClick={() => handleClose()}
              className={styles.addToCartButton}
              style={{
                margin: '5px auto',
                display: 'block',
                width: '90%',
                background: 'white',
                color: '#fc5447',
                border: '2px solid #fc5447',
              }}
            >
              Continue Shopping
            </button>
          </Col>
          <Col>
            <button
              className={styles.addToCartButton}
              style={{
                margin: '5px auto',
                display: 'block',
                width: '90%',
              }}
              onClick={() => router.push('/products/checkout')}
            >
              Checkout
            </button>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  )
}

export default ContinueShoppingModal
