import axios from 'axios'
import { useEffect, useState } from 'react'
import { Col, Form, Image, Modal, Row, Accordion, Card } from 'react-bootstrap'
import styles from '../index.module.css'
import DatePicker from 'react-datepicker'
// import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment'
import { updateOrders } from '../../../store/slices/productSlice'
import { useDispatch, useSelector } from 'react-redux'
import { v4 as uuid } from 'uuid'
import { toast } from 'react-toastify'

const QuickViewModal = ({
  show,
  handleClose,
  selectedProduct,
  openContinueModal,
}) => {
  const {
    name,
    selling_price,
    brand_name,
    size,
    quantity,
    unit_in_package,
    images,
    id,
    description,
    weight,
    weight_unit,
    sku,
  } = selectedProduct
  const [selectedImage, setSelectedImage] = useState({})
  const [orders, setOrders] = useState([
    {
      id: uuid(),
      date: new Date(),
      products: {
        name: name,
        quantity: 1,
        id,
        images,
        description,
        selling_price,
        brand_name,
      },
    },
  ])
  const dispatch = useDispatch()
  useEffect(() => {
    const initialValue = images ? images[0] : {}
    setSelectedImage(initialValue)
  }, [images])

  const addMoreItems = () => {
    const orderArray = [
      ...orders,
      {
        id: uuid(),
        date: new Date(),
        products: {
          name: name,
          quantity: 1,
          id,
          images,
          description,
          selling_price,
          brand_name,
        },
      },
    ]
    setOrders(orderArray)
  }
  const dateChangeHandler = (date, orderId) => {
    const dateArr = orders?.map((order) => {
      if (order.id === orderId) {
        order.date = date
      }
      return order
    })
    setOrders(dateArr)
  }
  const quantityAdd = (orderId) => {
    const arr1 = orders?.map((order) => {
      if (order.id === orderId) {
        order.products.quantity = parseInt(order.products.quantity) + 1
      }
      return order
    })
    setOrders(arr1)
  }
  const quantityMinus = (orderId) => {
    const arr = orders?.map((order) => {
      if (order.id === orderId) {
        order.products.quantity = parseInt(order.products.quantity) - 1
      }
      return order
    })
    setOrders(arr)
  }
  const removeOrder = (orderId) => {
    const reArr = orders?.filter((order) => order.id !== orderId)
    setOrders(reArr)
  }
  return (
    <Modal
      show={show}
      onHide={handleClose}
      fullscreen="md-down"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
      size="lg"
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <button
          onClick={() => handleClose()}
          className={styles.modalCloseButton}
        >
          <Image src="/images/close_small.svg" alt="X" />
        </button>
        <Row>
          <Col>
            <div className={styles.mainItemImage}>
              <Image
                src={selectedImage.url}
                alt=""
                height="200px"
                width="200px"
              />
            </div>
            {images?.map((image) => (
              <span
                key={image.url}
                style={
                  selectedImage.url === image.url
                    ? {
                        display: 'inline-block',
                        margin: '6px 4px 6px 0',
                        border: '2px solid #e3e3e3',
                        cursor: 'pointer',
                      }
                    : {
                        display: 'inline-block',
                        margin: '6px 4px 6px 0',
                        cursor: 'pointer',
                      }
                }
              >
                <Image
                  src={image.url}
                  alt=""
                  width="50px"
                  height="50px"
                  onClick={() => setSelectedImage(image)}
                />
              </span>
            ))}
          </Col>
          <Col>
            <h2 className={styles.productNameView}>{name}</h2>
            <h2 className={styles.productPriceView}>${selling_price}</h2>
            <h3 style={{ fontSize: '18px', lineHeight: '22px' }}>
              Especificaciones del producto:
            </h3>
            <ul className={styles.cartItemsDesc}>
              <li>
                Marca:{' '}
                <span
                  style={{ fontSize: '16px', fontWeight: '400', color: '#000' }}
                >
                  {brand_name}
                </span>
              </li>
              <li>
                Peso:{' '}
                <span
                  style={{ fontSize: '16px', fontWeight: '400', color: '#000' }}
                >
                  {weight} {weight_unit}
                </span>
              </li>
              <li>
                Cantidad:{' '}
                <span
                  style={{ fontSize: '16px', fontWeight: '400', color: '#000' }}
                >
                  {quantity}
                </span>
              </li>
              <li>
                Piezas por paquete:{' '}
                <span
                  style={{ fontSize: '16px', fontWeight: '400', color: '#000' }}
                >
                  {unit_in_package}
                </span>
              </li>
            </ul>
          </Col>
        </Row>
        {orders?.map((order) => (
          <div key={order.id} className={styles.cartItemsDiv}>
            {orders.length > 1 && (
              <button
                onClick={() => removeOrder(order.id)}
                className={styles.removeOrderButton}
              >
                <i className="fa fa-trash" aria-hidden="true"></i>
              </button>
            )}
            <div
              style={{ position: 'relative', width: '50%', margin: '0px auto' }}
            >
              <DatePicker
                selected={order.date}
                onChange={(date) => dateChangeHandler(date, order.id)}
                minDate={moment().toDate()}
                placeholderText="Select a day"
                className={styles.datePickerClass}
              />
              <span className={styles.calendarIcon}>
                <i className="fa fa-calendar" aria-hidden="true"></i>
              </span>
            </div>
            <div className={styles.quantityDiv}>
              <button
                onClick={() => quantityMinus(order.id)}
                disabled={order.products.quantity === 1}
                className={styles.quantityButton}
              >
                -
              </button>
              <span>{order.products.quantity}</span>
              <button
                onClick={() => quantityAdd(order.id)}
                className={styles.quantityButton}
              >
                +
              </button>
            </div>
          </div>
        ))}
        <button
          type="button"
          onClick={() => addMoreItems()}
          className={styles.addMoreItemsButton}
        >
          Agregar fechas
        </button>
        <Row>
          <button
            onClick={() => {
              const newOrder = orders?.map((order) => {
                return {
                  date: moment(order.date).format('YYYY-MM-DD'),
                  products: {
                    name: name,
                    quantity: order.products.quantity,
                    id,
                    images,
                    description,
                    selling_price,
                    brand_name,
                    size,
                    unit_in_package,
                    weight,
                    weight_unit,
                  },
                }
              })
              dispatch(updateOrders(newOrder))
              toast('Added to cart.')
              handleClose()
              setOrders([
                {
                  id: uuid(),
                  date: new Date(),
                  products: {
                    name: name,
                    quantity: 1,
                    id,
                    images,
                    description,
                    selling_price,
                    brand_name,
                  },
                },
              ])
            }}
            className={styles.addToCartButton}
          >
            Añadir al carrito
            <span style={{ float: 'right', marginRight: '5%' }}>
              <i className="fa fa-shopping-cart" aria-hidden="true"></i>
            </span>
          </button>
        </Row>
      </Modal.Body>
    </Modal>
  )
}

export default QuickViewModal
