import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { Col, Image, Row, Form, Button, InputGroup } from 'react-bootstrap'
import styles from './index.module.css'
import { useSelector, useDispatch } from 'react-redux'
import QuickViewModal from './Modals/QuickViewModal'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import { v4 as uuid } from 'uuid'
import { updateOrders } from '../../store/slices/productSlice'
import { useRouter } from 'next/dist/client/router'
import { toast } from 'react-toastify'
import Sidebar from './Sidebar'
import Pagination from 'react-js-pagination'
import withAuth from '../../hoc/withAuth'

const Products = () => {
  const [displayFilterMenu, setDisplayFilterMenu] = useState(false)
  const [displaySortMenu, setDisplaySortMenu] = useState(false)
  const [filterKey, setFilterKey] = useState(null)
  const [sortKey, setSortKey] = useState(null)
  const [sortDir, setSortDir] = useState(true)
  const [showQuickViewModal, setShowQuickViewModal] = useState(false)
  const [products, setProducts] = useState([])
  const [selectedProduct, setSelectedProduct] = useState({})
  const [orders, setOrders] = useState([])
  const [windowWidth, setWindowWidth] = useState(0)
  const [showSidebar, setShowSidebar] = useState(false)
  const [brands, setBrands] = useState([])
  const [categories, setCategories] = useState([])
  const [brandId, setBrandId] = useState('')
  const [categoryId, setCategoryId] = useState('')
  const [activePage, setActivePage] = useState(1)
  const [total, setTotal] = useState(0)

  const dispatch = useDispatch()
  const router = useRouter()
  const { orders: ordersRedux } = useSelector((state) => state.product)
  useEffect(() => {
    setWindowWidth(window.innerWidth)
  }, [])
  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplayFilterMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const wrapperRef = useRef(null)
  useOutsideAlerter(wrapperRef)
  function useOutsideAlerterSort(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplaySortMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const sortRef = useRef(null)
  useOutsideAlerterSort(sortRef)

  const getAllBrands = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/brands`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setBrands(res.data.brands))
  }
  const getAllCategories = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/categories`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setCategories(res.data.categories))
  }

  // Get All Products
  const getAllProducts = (
    query,
    page = 1,
    brandFilterId,
    categoryFilterId,
    sortKeyValue,
    sortDirection
  ) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/products?_limit=30&_page=${page}`
    if (query) {
      url += `&name=${encodeURIComponent(query)}`
    }
    if (brandFilterId) {
      url += `&brand_id=${brandFilterId}`
    }
    if (categoryFilterId) {
      url += `&category_id=${categoryFilterId}`
    }
    if (sortKeyValue) {
      const dir = sortDirection === true ? 'desc' : 'asc'
      url += `&sort_by=${sortKeyValue}&sort_dir=${dir}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setProducts(res.data.products)
      setTotal(res.data.total)
      const orderArray = res.data.products?.map((prod) => {
        const { name, id, images, description, selling_price, brand_name } =
          prod
        return {
          productId: id,
          id: uuid(),
          date: new Date(),
          products: {
            name,
            quantity: 1,
            id,
            images,
            description,
            selling_price,
            brand_name,
          },
        }
      })
      setOrders(orderArray)
    })
  }

  useEffect(() => {
    getAllProducts()
    getAllBrands()
    getAllCategories()
  }, [])

  const filterKeyChangeHandler = (e) => {
    const { value } = e.target
    if (e.target.checked) {
      setFilterKey(value)
      getAllProducts(null, 1, value)
    } else {
      setFilterKey(null)
      getAllProducts(null, 1, null)
    }
    setDisplayFilterMenu(false)
  }
  const sortKeyChangeHandler = (dir, e) => {
    const { value } = e.target
    if (e.target.checked) {
      setSortKey(value)
      setSortDir(dir)
      getAllProducts(null, 1, brandId, categoryId, value, dir)
    } else {
      setSortKey(null)
      getAllProducts(null, 1, brandId, categoryId, null)
    }
    setDisplaySortMenu(false)
  }
  const brandIdChangeHandler = (id) => {
    setBrandId(id)
    getAllProducts(null, 1, id, categoryId, sortKey, sortDir)
  }
  const categoryIdChangeHandler = (id) => {
    setCategoryId(id)
    getAllProducts(null, 1, brandId, id, sortKey, sortDir)
  }
  const dateChangeHandler = (date, orderId) => {
    const dateArr = orders?.map((order) => {
      if (order.id === orderId) {
        order.date = date
      }
      return order
    })
    setOrders(dateArr)
  }
  const quantityAdd = (orderId) => {
    const arr1 = orders?.map((order) => {
      if (order.id === orderId) {
        order.products.quantity = parseInt(order.products.quantity) + 1
      }
      return order
    })
    setOrders(arr1)
  }
  const clearFilter = () => {
    getAllProducts(null, 1, null, null, sortKey, sortDir)
    setBrandId('')
    setCategoryId('')
  }
  const quantityMinus = (orderId) => {
    const arr = orders?.map((order) => {
      if (order.id === orderId) {
        order.products.quantity = parseInt(order.products.quantity) - 1
      }
      return order
    })
    setOrders(arr)
  }
  const allProductsArray = ordersRedux?.map((order) => order.products)
  const allProducts = allProductsArray?.reduce(function (a, b) {
    return a.concat(b)
  }, [])
  let totalProductsQuantity = 0
  for (let i = 0; i < allProducts.length; i++) {
    totalProductsQuantity += allProducts[i].quantity
  }
  return (
    <div className={styles.Container}>
      <Row>
        <Col>
          <div style={{ position: 'relative' }}>
            <input
              className={styles.searchInput}
              placeholder="Buscar productos"
              onChange={(e) => getAllProducts(e.target.value)}
            />
            <Image
              src="/images/searchIcon.png"
              alt=""
              height="16px"
              width="16px"
              className={styles.searchInputIcon}
            />
          </div>
        </Col>
        <Col>
          <div className={styles.filterSortDiv}>
            <button
              className={styles.myCartButton}
              onClick={() => router.push('/products/checkout')}
            >
              <i className="fa fa-shopping-cart" aria-hidden="true"></i>
              <span className={styles.myCartButtonQuantity}>
                <div style={{ transform: 'translateY(-3px)' }}>
                  {totalProductsQuantity}
                </div>
              </span>
            </button>
            <div className={styles.sortByDiv}>
              <div onClick={() => setDisplaySortMenu(!displaySortMenu)}>
                Ordenar por{' '}
                <span style={{ float: 'right' }}>
                  <i className="fa fa-caret-down" aria-hidden="true"></i>
                </span>
              </div>
              {displaySortMenu ? (
                <ul ref={sortRef}>
                  <li
                    className={
                      sortKey === 'selling_price' &&
                      !sortDir &&
                      styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="selling_price"
                          checked={sortKey === 'selling_price' && !sortDir}
                          onChange={(e) => sortKeyChangeHandler(false, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Precio ( Menor a mayor)
                    </label>
                  </li>
                  <li
                    className={
                      sortKey === 'selling_price' &&
                      sortDir &&
                      styles.sortByDivLi
                    }
                    style={{ padding: '12px' }}
                  >
                    <label>
                      <span
                        style={{ border: 'none', background: 'transparent' }}
                      >
                        <input
                          type="checkbox"
                          value="selling_price"
                          checked={sortKey === 'selling_price' && sortDir}
                          onChange={(e) => sortKeyChangeHandler(true, e)}
                          style={{ display: 'none' }}
                        />
                      </span>
                      Precio (Mayor a menor)
                    </label>
                  </li>
                </ul>
              ) : null}
            </div>
            <div
              className={styles.filterByDiv}
              onClick={() => setShowSidebar(!showSidebar)}
            >
              <span onClick={() => setDisplayFilterMenu(!displayFilterMenu)}>
                Filtrar por
              </span>
            </div>
          </div>
        </Col>
      </Row>
      {showSidebar && (
        <Sidebar
          setShowSidebar={setShowSidebar}
          brands={brands}
          categories={categories}
          brandId={brandId}
          categoryId={categoryId}
          brandIdChangeHandler={brandIdChangeHandler}
          categoryIdChangeHandler={categoryIdChangeHandler}
          clearFilter={clearFilter}
        />
      )}
      <div className={styles.ProductsGrid}>
        {products?.map((product, index) => {
          const {
            id,
            name,
            selling_price,
            description,
            images,
            brand_name,
            size,
            quantity,
            unit_in_package,
            weight,
            weight_unit,
            category_name,
          } = product
          return (
            <div className={styles.productsCard} key={id}>
              <div className={styles.productsCardImg}>
                {images.length !== 0 && (
                  <Image
                    src={images[0].url}
                    alt=""
                    width="100%"
                    height="110px"
                  />
                )}
                <button
                  type="button"
                  className={styles.productsCardButton}
                  onClick={() => {
                    setShowQuickViewModal(true)
                    setSelectedProduct(product)
                  }}
                >
                  Vista rápida
                </button>
              </div>
              <h2 className={styles.productsCardBrand}>{brand_name}</h2>
              <h2 className={styles.productsCardName}>{name}</h2>
              <h2 className={styles.productsCardCategory}>{category_name}</h2>
              <p className={styles.productsCardDesc}>
                <span
                  style={{
                    fontSize: '12px',
                    lineHeight: '16px',
                    color: '#0F1111',
                    fontWeight: '500',
                  }}
                >
                  Peso:{' '}
                </span>
                {weight} {weight_unit}
              </p>
              <p className={styles.productsCardDesc}>
                <span
                  style={{
                    fontSize: '12px',
                    lineHeight: '16px',
                    color: '#0F1111',
                    fontWeight: '500',
                  }}
                >
                  Piezas por paquete:{' '}
                </span>
                {unit_in_package}
              </p>
              <p className={styles.productsCardPrice}>${selling_price}</p>
              <div className={styles.productsOperation}>
                <div
                  style={{
                    position: 'relative',
                    margin: '0px auto',
                  }}
                >
                  <DatePicker
                    selected={orders[index]?.date}
                    onChange={(date) =>
                      dateChangeHandler(date, orders[index]?.id)
                    }
                    minDate={moment().toDate()}
                    placeholderText="Select a day"
                    className={styles.datePickerClassCard}
                  />
                  <span className={styles.calendarIcon}>
                    <i className="fa fa-calendar" aria-hidden="true"></i>
                  </span>
                </div>
                <div className={styles.quantitySubmitDiv}>
                  <div className={styles.quantityDiv}>
                    <button
                      onClick={() => quantityMinus(orders[index]?.id)}
                      disabled={orders[index]?.products.quantity === 1}
                      className={styles.quantityButton}
                    >
                      -
                    </button>
                    <span>{orders[index]?.products.quantity}</span>
                    <button
                      onClick={() => quantityAdd(orders[index]?.id)}
                      className={styles.quantityButton}
                    >
                      +
                    </button>
                  </div>
                  <button
                    onClick={() => {
                      setSelectedProduct(product)
                      const orderObject = [
                        orders.find((order) => order.productId === id),
                      ]
                      const newOrder = orderObject?.map((order) => {
                        return {
                          date: moment(order.date).format('YYYY-MM-DD'),
                          products: {
                            name: name,
                            quantity: order.products.quantity,
                            id,
                            images,
                            description,
                            selling_price,
                            brand_name,
                            size,
                            unit_in_package,
                            weight,
                            weight_unit,
                          },
                        }
                      })
                      dispatch(updateOrders(newOrder))
                      toast('Added to cart.')
                    }}
                    className={styles.addToCartButtonCard}
                  >
                    {windowWidth > 800 && 'Add'}{' '}
                    <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                  </button>
                </div>
              </div>
            </div>
          )
        })}
      </div>
      <div
        style={{
          marginTop: '20px',
          borderTop: '1px solid #e7e7e7',
          textAlign: 'right',
          paddingTop: '20px',
          minHeight: '62px',
        }}
      >
        <div style={{ float: 'right' }}>
          <Pagination
            activePage={activePage}
            itemsCountPerPage={30}
            totalItemsCount={total}
            pageRangeDisplayed={5}
            onChange={(page) => {
              setActivePage(page)
              getAllProducts(null, page, brandId, categoryId, sortKey, sortDir)
            }}
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>
      </div>
      <QuickViewModal
        show={showQuickViewModal}
        onHide={() => setShowQuickViewModal(false)}
        handleClose={() => setShowQuickViewModal(false)}
        selectedProduct={selectedProduct}
        openContinueModal={() => setShowContinueModal(true)}
      />
    </div>
  )
}

export default withAuth(Products)
