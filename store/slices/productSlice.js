import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  orders: [],
}

const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    loadState(state, action) {
      const serializedState = localStorage.getItem('orders')
      console.log(JSON.parse(serializedState), 'kk')
      state.orders =
        serializedState === null ? state.orders : JSON.parse(serializedState)
    },
    setInitialState(state, action) {
      state.orders = []
    },
    updateOrders(state, action) {
      const dateList = state.orders?.map((order) => order.date)
      if (state.orders.length === 0) {
        state.orders = action.payload?.map((pay) => {
          return {
            ...pay,
            products: [pay.products],
          }
        })
      } else {
        for (let i = 0; i < action.payload.length; i++) {
          if (dateList.includes(action.payload[i].date)) {
            state.orders?.map((order) => {
              const prodList = order.products.map((prod) => prod.id)
              if (prodList.includes(action.payload[i].products.id)) {
                order.date === action.payload[i].date
                  ? (order.products = order.products?.map((prod) => {
                      return {
                        ...prod,
                        quantity:
                          action.payload[i].products.id === prod.id
                            ? parseInt(prod.quantity) +
                              parseInt(action.payload[i].products.quantity)
                            : prod.quantity,
                      }
                    }))
                  : [...order.products]
              } else {
                order.products =
                  order.date === action.payload[i].date
                    ? [...order.products, action.payload[i].products]
                    : [...order.products]
              }
            })
          } else {
            state.orders = [
              ...state.orders,
              {
                ...action.payload[i],
                products: [action.payload[i].products],
              },
            ]
          }
        }
      }
      localStorage.setItem('orders', JSON.stringify(state.orders))
    },
    removeProduct(state, action) {
      state.orders = state.orders?.map((order) => {
        if (order.date === action.payload.date) {
          order.products = order.products?.filter(
            (prod) => prod.id !== action.payload.productId
          )
        }
        return order
      })
      state.orders = state.orders?.filter(
        (order) => order.products.length !== 0
      )
      localStorage.setItem('orders', JSON.stringify(state.orders))
    },
    updateQuantity(state, action) {
      state.orders = state.orders?.map((order) => {
        if (order.date === action.payload.date) {
          order.products = order.products?.map((prod) => {
            if (prod.id === action.payload.productId) {
              prod.quantity =
                action.payload.action === 'add'
                  ? parseInt(prod.quantity) + 1
                  : parseInt(prod.quantity) - 1
            }
            return prod
          })
        }
        return order
      })
      localStorage.setItem('orders', JSON.stringify(state.orders))
    },
  },
})

export const {
  updateOrders,
  removeProduct,
  updateQuantity,
  loadState,
  setInitialState,
} = productSlice.actions
export default productSlice.reducer
